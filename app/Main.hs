module Main where

import Lib

main :: IO ()
main = do
    name <- getLine
    putStrLn (greet name)

greet :: String -> String
greet name = "hello " ++ name ++ "!"

-- leap year
-- every year that is evenly divisible by 4 is a leap year
-- except every year that is evenly divisible by 100
-- unless the year is also evenly divisible by 400

isLeapYear :: Int -> Bool
isLeapYear n =
    if (mod n 4) == 0
        then if (mod n 100) == 0
            then (mod n 400) == 0
            else True
        else False